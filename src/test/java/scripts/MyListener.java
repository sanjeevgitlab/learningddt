package scripts;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.WebDriverListener;

public class MyListener implements WebDriverListener{
	

	public void afterSendKeys(WebElement element) {
		System.out.println("Testing Listener");
	}
	
	public void afterQuit(WebDriver driver) {
		System.out.println("Testing Listener");
	}

}
