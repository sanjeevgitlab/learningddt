package scripts;

import java.io.File;
import java.io.IOException;

import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.events.EventFiringDecorator;
import org.openqa.selenium.support.events.WebDriverListener;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.google.common.io.Files;

public class DDTExample {
	WebDriver originalDriver, driver;
	Logger log;
	WebDriverListener myListener;
	TakesScreenshot ts;
  @Test(dataProvider = "dpForXLS" , dataProviderClass = AllDataProviders.class)
  public void f(String	userName, String password) throws InterruptedException, IOException {
	  	DOMConfigurator.configure("log4j-alternate.xml");
	  	log = Logger.getLogger(DDTExample.class.getName());
	  	log.info("Opening the browser");
	  	driver.get("https://nichethyself.com/tourism/home.html");
		WebElement username = driver.findElement(By.name("username"));
		username.sendKeys(userName);
		driver.findElement(By.name("password")).sendKeys(password);
		Thread.sleep(2000);
		File srcFile = ts.getScreenshotAs(OutputType.FILE);
		try {
			Files.copy(srcFile, new File("C:\\Users\\Sanjeev\\Documents\\New Selenium Training\\Selenium Training\\Classes\\12_17-Jul-22\\17-Jul-22 (1)\\ScreenshotsFromProgram\\PassedWith"+userName+"_"+password+".png"));
		} catch (IOException e) {
			File srcFile1 = ts.getScreenshotAs(OutputType.FILE);
			Files.copy(srcFile1, new File("C:\\Users\\Sanjeev\\Documents\\New Selenium Training\\Selenium Training\\Classes\\12_17-Jul-22\\17-Jul-22 (1)\\ScreenshotsFromProgram\\FailedWith"+userName+"_"+password+".png"));
			e.printStackTrace();
		}
		username.submit();
		
	  
  }
  
  //@Test(dataProvider = "dpForXLSX" , dataProviderClass = AllDataProviders.class)
  public void f1(String	userName, String password) throws InterruptedException {
	  driver.get("https://nichethyself.com/tourism/home.html");
		WebElement username = driver.findElement(By.name("username"));
		username.sendKeys(userName);
		driver.findElement(By.name("password")).sendKeys(password);
		Thread.sleep(2000);
		username.submit();
	  
  }
  
  //@Test(dataProvider = "dpForCSV" , dataProviderClass = AllDataProviders.class)
  public void f2(String	userName, String password) throws InterruptedException {
	  driver.get("https://nichethyself.com/tourism/home.html");
		WebElement username = driver.findElement(By.name("username"));
		username.sendKeys(userName);
		driver.findElement(By.name("password")).sendKeys(password);
		Thread.sleep(2000);
		username.submit();
	  
  }
  
  @BeforeMethod
  public void beforeMethod() {
	  System.setProperty("webdriver.chrome.driver", "C:\\Users\\Sanjeev\\eclipse-workspace-selenium\\learningDDT\\src\\test\\resources\\drivers\\chromedriver.exe");
	  originalDriver = new ChromeDriver();
	  //myListener = new MyListener();
	  myListener = new MyListener();
	  driver = new EventFiringDecorator(myListener).decorate(originalDriver);
	  ts = (TakesScreenshot) driver;
	  
  }

  @AfterMethod
  public void afterMethod() {
	  driver.quit();
  }


  
}
