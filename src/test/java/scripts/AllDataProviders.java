package scripts;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.DataProvider;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

//Data Provider public java.util.List scripts.AllDataProviders.dpForCSV() must return either Object[][] or Object[] or Iterator<Object[]> or Iterator<Object>

public class AllDataProviders {

	@DataProvider
	public Object[][] dpForXLS() {
		return getTestDataFromXLS("C:\\Users\\Sanjeev\\eclipse-workspace-selenium\\learningDDT\\src\\test\\resources\\data\\Credentials.xls", "NT", "NTStartEnd");

	}
	
	@DataProvider
	public Object[][] dpForXLSX() {
		return getTestDataFromXLSX("C:\\Users\\Sanjeev\\eclipse-workspace-selenium\\learningDDT\\src\\test\\resources\\data\\CredentialsXLSX.xlsx", "NT");

	}
	
	@DataProvider
	public Iterator<String[]> dpForCSV() {
		Collection<String[]> retObjArr =  getTestDataFromCSV("C:\\\\Users\\\\Sanjeev\\\\eclipse-workspace-selenium\\\\learningDDT\\\\src\\\\test\\\\resources\\\\data\\\\Credentials.csv");
		return (retObjArr.iterator());
	}
	
	@DataProvider
	public Iterator<String[]> dpForDB() {
		Collection<String[]> retObjArr =  getTestDataFromCSV("C:\\\\Users\\\\Sanjeev\\\\eclipse-workspace-selenium\\\\learningDDT\\\\src\\\\test\\\\resources\\\\data\\\\Credentials.csv");
		return (retObjArr.iterator());
	}
	

	private Collection<String[]> getTestDataFromCSV(String string) {
		FileReader out=null;
		List<String[]> list = new ArrayList<String[]>();
		String line;
		BufferedReader br = null;
		try {
			out = new FileReader(string);
			br = new BufferedReader(out);
			while((line = br.readLine()) != null){
				String[] field = line.split(",");
				System.out.println(Arrays.toString(field));
				list.add(field);
			}
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
			try {
				out.close();
				br.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return list;
	}

	private String[][] getTestDataFromXLSX(String xlFilePath, String sheetName) {
		
		String[][] testData = null;
		File file = new File(xlFilePath);
		
		
		try {
			FileInputStream fis = new FileInputStream(file);
			XSSFWorkbook wb = new XSSFWorkbook(fis);
			XSSFSheet sheet =  wb.getSheet(sheetName);
			int rowCount = sheet.getLastRowNum() - sheet.getFirstRowNum();
			System.out.println(sheet.getLastRowNum() + " and " + sheet.getFirstRowNum());
			System.out.println(rowCount);
			//int actualRowCount = rowCount -1;
			//System.out.println(actualRowCount);
			int cellCount = sheet.getRow(0).getLastCellNum();
			System.out.println(cellCount);
			testData = new String[rowCount][cellCount];
			
			
			int ci = 0;
			for (int i = 1; i <= sheet.getLastRowNum(); i++, ci++) {

				int cj = 0;
				for (int j = 0; j < cellCount; j++, cj++) {
					testData[ci][cj] = sheet.getRow(i).getCell(j).toString();
				}

			}
			wb.close();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return testData;
	}

	public String[][] getTestDataFromXLS(String xlFilePath, String sheetName, String marker) {

		String[][] testData = null;
		File file = new File(xlFilePath);
		try {
			Workbook wb = Workbook.getWorkbook(file);
			Sheet sheet = wb.getSheet(sheetName);
			Cell markerStart = sheet.findCell(marker);

			int markerStartRow = markerStart.getRow();
			int markerStartColumn = markerStart.getColumn();

			Cell markerEnd = sheet.findCell(marker, markerStartColumn + 1, markerStartRow + 1, 100, 100, false);

			int markerEndRow = markerEnd.getRow();
			int markerEndColumn = markerEnd.getColumn();

			testData = new String[markerEndRow - markerStartRow - 1][markerEndColumn - markerStartColumn - 1];
			int ci = 0;
			for (int i = markerStartRow + 1; i < markerEndRow; i++, ci++) {

				int cj = 0;
				for (int j = markerStartColumn + 1; j < markerEndColumn; j++, cj++) {
					testData[ci][cj] = sheet.getCell(j, i).getContents();
				}

			}

		} catch (BiffException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return testData;

	}

}
